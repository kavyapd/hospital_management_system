<?php
namespace App;

/**
* class MSG91 to send SMS on Mobile Numbers.
* @author 
*/
class MSG91 {
    private $API_KEY = '298933A6I7qr0fCN1G5da583ef';
    private $SENDER_ID = "Verify";
    private $ROUTE_NO = 4;
    private $RESPONSE_TYPE = 'json';
    
    function __construct() {

    }
    
    public function sendSMS($OTP, $mobileNumber){
        $isError = 0;
        $curl = curl_init();
        
        //Preparing post parameters
        $postData = array(
            'authkey' => $this->API_KEY,
            'mobile' => $mobileNumber,
            'otp'=>$OTP,
            'sender' => $this->SENDER_ID,
            'route' => $this->ROUTE_NO,
            'response' => $this->RESPONSE_TYPE
        );
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://control.msg91.com/api/sendotp.php",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $postData,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));

        
        $output = curl_exec($curl);
     
        //Print error if any
        if (curl_errno($curl)) {
            $isError = true;
            $errorMessage = curl_error($ch);
        }
        curl_close($curl);
        if($isError){
            return array('error' => 1 , 'message' => $errorMessage);
        }else{
            return array('error' => 0 );
        }
    }
}
?>