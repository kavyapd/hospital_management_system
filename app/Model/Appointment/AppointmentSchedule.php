<?php

namespace App\Model\Appointment;

use Illuminate\Database\Eloquent\Model;

class AppointmentSchedule extends Model
{
    protected $table = 'appointment_scheduled_information';
     
    protected $guarded = ['id'];
    
    protected $hidden = [];
}
