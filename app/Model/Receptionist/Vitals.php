<?php

namespace App\Model\Receptionist;

use Illuminate\Database\Eloquent\Model;

class Vitals extends Model
{
    protected $guarded = ['id'];
    
    protected $hidden = [];
}
