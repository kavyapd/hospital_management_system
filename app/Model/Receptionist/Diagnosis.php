<?php

namespace App\Model\Receptionist;

use Illuminate\Database\Eloquent\Model;

class Diagnosis extends Model
{
    protected $table = 'diagnosis';

    protected $guarded = ['id'];
    
    protected $hidden = [];
}
