<?php

namespace App\Model\Receptionist;

use Illuminate\Database\Eloquent\Model;

class PatientInformation extends Model
{
    protected $guarded = ['id'];
    
    protected $hidden = [];
}
