<?php

namespace App\Model\Receptionist;

use Illuminate\Database\Eloquent\Model;

class PatientDocuments extends Model
{
    protected $guarded = ['id'];
    
    protected $hidden = [];
}
