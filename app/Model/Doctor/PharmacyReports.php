<?php

namespace App\Model\Doctor;

use Illuminate\Database\Eloquent\Model;

class PharmacyReports extends Model
{
    protected $guarded = ['id'];
    
    protected $hidden = [];
}
