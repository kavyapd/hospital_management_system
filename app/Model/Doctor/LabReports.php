<?php

namespace App\Model\DOctor;

use Illuminate\Database\Eloquent\Model;

class LabReports extends Model
{
    protected $guarded = ['id'];
    
    protected $hidden = [];
}
