<?php

namespace App\Model\Register;

use Illuminate\Database\Eloquent\Model;

class MobileVerified extends Model
{
    protected $table = 'mobile_verified';

    protected $guarded = ['id'];
    
    protected $hidden = [];
}
