<?php

namespace App\Model\Register;

use Illuminate\Database\Eloquent\Model;

class PasswordResets extends Model
{
    protected $guarded = ['id'];
    
    protected $hidden = [];
}
