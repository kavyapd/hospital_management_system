<?php

namespace App\Model\Register;

use Illuminate\Database\Eloquent\Model;

class EmailVerified extends Model
{
    protected $table = 'email_verified';

    protected $guarded = ['id'];
    
    protected $hidden = [];
}
