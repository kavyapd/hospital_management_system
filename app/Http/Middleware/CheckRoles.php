<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $roles_result = DB::table('users')
                    ->select('users.id')
                    ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                    ->where('users.id',Auth::user()->id)
                    ->where('roles.id','!=',1)
                    ->get();
            $roles_result = json_decode($roles_result, true);
            if(!empty($roles_result) && is_array($roles_result) && sizeof($roles_result) > 0){
                return $next($request);
            }else{
                return response()->json([
                    'message' => 'Unauthorized'
                ], 401);
            }
        }else{
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }   
    }
}
