<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $admin_role_result = DB::table('users')
                    ->select('users.id')
                    ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                    ->where('users.id',Auth::user()->id)
                    ->where('roles.role','admin')
                    ->get();
            $admin_role_result = json_decode($admin_role_result, true);
            if(!empty($admin_role_result) && is_array($admin_role_result) && sizeof($admin_role_result) > 0){
                return $next($request);
            }else{
                return response()->json([
                    'message' => 'Unauthorized'
                ], 401);
            }
        }else{
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }   
    }
}
