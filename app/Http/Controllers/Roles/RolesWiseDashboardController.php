<?php

namespace App\Http\Controllers\Roles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RolesWiseDashboardController extends Controller
{
//get user details w.r.to user login 
    public function getRolesDetails(Request $request){
        if (Auth::check()){
            $roles_details = DB::table('users')
                ->select('users.id','users.user_name','users.email','users.first_name','users.last_name','roles.role_name','hospitals.hospital_name')
                ->leftJoin('roles','roles.id','=','users.role_id')
                ->leftJoin('hospitals','hospitals.id','=','users.hospital_id')
                ->where('users.id',Auth::user()->id)
                ->where('users.active_status',1)
                ->get(); 
            return response()->json([
                'message' => $roles_details,
                'success' => true,
            ], 200);
        }
    }
//update user details w.r.to user login 
    public function updateRolesDetails(Request $request){
        if (Auth::check()){
            $request->validate([
                'id' => array('required','integer','exists:users,id',new \App\Rules\CheckRole()),
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'hospital_id'=>'required|exists:hospitals,id|integer',
            ]);
            DB::beginTransaction();
            $roles_updated_result = DB::table('users')
                ->where('id',$request->id)
                ->update([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'hospital_id' =>$request->hospital_id,
                    'updated_at' => Carbon::now()
                ]);
            if ($roles_updated_result){  
                 DB::commit();
                return response()->json([
                    'message' => 'User updated successfully',
                    'success' => true,
                ], 200);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 400);
        }
    }
//delete user details w.r.to user login 
    public function deleteRole(Request $request){
        if (Auth::check()){
            if ($request->route('id')){
                $role_check_result = DB::table('users')
                    ->select('users.id')
                    ->leftJoin('roles','roles.id','=','users.role_id')
                    ->where('users.id',$request->route('id'))
                    ->where('users.active_status',1)
                    ->where('users.admin_status',1)
                    ->get();
                $role_check_result = json_decode($role_check_result);
                if (!empty($role_check_result) && is_array($role_check_result) && sizeof($role_check_result)){
                    DB::beginTransaction();
                    $deleted_role_affected = DB::table('users')
                            ->where('id',$request->route('id'))
                            ->where('active_status',true)
                            ->update(['active_status'=>false,'admin_status'=>false]);
                    if ($deleted_role_affected){
                        DB::commit();
                        return response()->json([
                            'message' => 'User deleted successfully',
                            'success' => true,
                        ], 200);
                    }
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => false,
                    ], 200);
                }else{
                    return response()->json([
                        'message' => 'User not found',
                        'success' => false,
                    ], 404);
                }
            }else{
                return response()->json([
                    'message' => 'User ID is missing',
                    'success' => false,
                ], 400);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
}
