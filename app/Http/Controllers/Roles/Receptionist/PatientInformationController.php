<?php

namespace App\Http\Controllers\Roles\Receptionist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\Receptionist\PatientInformation;
use App\Model\Appointment\AppointmentSchedule;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class PatientInformationController extends Controller
{
//create patient information    
    public function addPatientDetails(Request $request){
        if (Auth::check()){
            $request->validate([
                'patient_name' => 'required|string|max:255',
                'birth_date'=>'required|date|date_format:Y-m-d',
                'gender' => 'required|string|max:255',
                'marital_status' => 'required|string|max:255',
                'age' => 'required|integer',
                'occupation' => 'string|max:255',
                'mobile_number'=>'required|min:10|max:10',
                'email' => 'required|email|max:255',        
                'language' => 'string|max:255',
                'nationality' => 'required|string|max:255',
                'responsible_person' => 'required|string|max:255',
                'responsible_person_name' => 'required|string|max:255',
                'responsible_person_mobile_number'=>'required|min:10|max:10',
                'doctor_id' => 'required|max:255|exists:users,id|integer',
                'address'=>'required|string|max:255',
                'refered_by_doctor'=>'string|max:255',
                'appointment_date_time'=>'required|date|date_format:Y-m-d H:i',
            ]);
            DB::beginTransaction();
            $patient_insert_result = new PatientInformation([
               'created_by' => Auth::user()->id,
               'patient_name' => $request->patient_name,
               'birth_date' => $request->birth_date,
               'gender' => $request->gender,
               'marital_status' => $request->marital_status,
               'age' => $request->age,
               'occupation' => $request->occupation,
               'mobile_number' => $request->mobile_number,
               'email' => $request->email,
               'language' => $request->language,
               'nationality' => $request->nationality,
               'responsible_person' => $request->responsible_person,
               'responsible_person_name' => $request->responsible_person_name,
               'responsible_person_mobile_number' => $request->responsible_person_mobile_number,
               'address' => $request->address,
               'refered_by_doctor' => $request->refered_by_doctor,
            ]);
            if ($patient_insert_result->save()){
                if($patient_id = $patient_insert_result->id){
                    $startId = 00;
                    $year = date('Y');
                    DB::table('patient_information')->where('id',$patient_insert_result->id)->update(['patient_id'=> "HM".$year.($startId + $patient_id),]);
                    $doctor_result = DB::table('appointment_scheduled_information')
                            ->where('doctor_id',$request->doctor_id)
                            ->where('appointment_date_time',$request->appointment_date_time)
                            ->get();
                    $doctor_result = json_decode($doctor_result,true);
                    if(!empty($doctor_result) && is_array($doctor_result) && sizeof($doctor_result) > 0){
                        return response()->json([
                            'message' => 'Doctor has already appointment with this timings',
                            'success' => true,
                        ], 200);
                    }else{
                        $appstartId = 1000;
                        $appointment_schedule_details = new AppointmentSchedule([
                            'doctor_id'=>$request->doctor_id,
                            'patient_id'=>$patient_id,
                            'appointment_date_time'=>$request->appointment_date_time,
                            'appointment_status'=>'active',
                            'scheduled_by'=>Auth::user()->id,
                        ]);
                        if($appointment_schedule_details->save()){
                            if($appointment_id = $appointment_schedule_details->id){
                                DB::table('appointment_scheduled_information')->where('id',$appointment_schedule_details->id)->update(['appointment_id'=> ($appstartId + $appointment_id),]);
                                DB::commit();
                                return response()->json([
                                    'message' => 'Patient created successfully',
                                    'success' => true,
                                ], 200);
                            }
                        }
                    }
                }else{
                   return response()->json([
                       'message' => 'Something went wrong',
                       'success' => false,
                   ], 400);
                }
            }else{
                return response()->json([
                    'message' => 'Something went wrong',
                    'success' => false,
                ], 400);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
//update patient information    
    public function updatePatientDetails(Request $request){
        if(Auth::check()){
            $request->validate([
               'id' => 'required|integer|exists:patient_information,id',
               'patient_name' => 'required|string|max:255',
               'birth_date'=>'required|date|date_format:Y-m-d',
               'gender' => 'required|integer',
               'marital_status' => 'required|integer',
               'age' => 'required|integer',
               'occupation' => 'string|max:255',
               'mobile_number'=>'required|min:10|max:10',
               'email' => 'required|email|max:255',        
               'language' => 'string|max:255',
               'nationality' => 'required|string|max:255',
               'responsible_person' => 'integer',
               'responsible_person_name' => 'string|max:255',
               'responsible_person_mobile_number'=>'required|min:10|max:10',
               'address'=>'required|string|max:255',
               'refered_by_doctor'=>'string|max:255',
           ]);
            DB::beginTransaction();
            $patient_updated_result = DB::table('patient_information')
                ->where('id',$request->id)
                ->update([
                    'updated_by' => Auth::user()->id,
                    'patient_name' => $request->patient_name,
                    'birth_date' => $request->birth_date,
                    'gender' => $request->gender,
                    'marital_status' => $request->marital_status,
                    'age' => $request->age,
                    'occupation' => $request->occupation,
                    'mobile_number' => $request->mobile_number,
                    'email' => $request->email,
                    'language' => $request->language,
                    'nationality' => $request->nationality,
                    'responsible_person' => $request->responsible_person,
                    'responsible_person_name' => $request->responsible_person_name,
                    'responsible_person_mobile_number' => $request->responsible_person_mobile_number,
                    'address' => $request->address,
                    'refered_by_doctor' => $request->refered_by_doctor,
                    'updated_at' => Carbon::now()
                ]);
            if($patient_updated_result){  
                DB::commit();
                 return response()->json([
                     'message' => 'Patient details updated successfully',
                      'success' => true,
                 ], 200);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
//fetch patient information    
    public function getPatientDetails(Request $request){
        if (Auth::check()){
            $patient_details = DB::table('patient_information')
                 ->select('patient_information.id','patient_information.patient_id','patient_information.patient_name', 'patient_information.birth_date','patient_information.gender',
                        'patient_information.marital_status','patient_information.age','patient_information.occupation','patient_information.mobile_number','patient_information.email','patient_information.responsible_person_mobile_number',
                        'patient_information.language','patient_information.nationality','patient_information.responsible_person','patient_information.responsible_person_name','patient_information.address','patient_information.refered_by_doctor')
                 ->leftJoin('users','users.id','=','patient_information.created_by')
                 ->leftJoin('roles','roles.id','=','users.role_id')
                 ->where('roles.role','=','receptionist')
                  ->get();
             $patient_details = json_decode($patient_details,true);
             $completed_patient_details_array = array();
            if(!empty($patient_details) && is_array($patient_details) && sizeof($patient_details)){
                foreach ($patient_details as $key => $value) { 
                    $completed_patient_details_array[] = [
                            "id" => $value['id'],
                            "patient_id" => $value['patient_id'],
                            "patient_name" => $value['patient_name'],
                            "birth_date"=> $value['birth_date'],
                            "gender"=> $value['gender'],
                            "marital_status"=> $value['marital_status'],
                            "age"=> $value['age'],
                            "occupation"=> $value['occupation'],
                            "mobile_number"=> $value['mobile_number'],
                            "email"=> $value['email'],
                            "language"=> $value['language'],
                            "nationality"=> $value['nationality'],
                            "responsible_person"=> $value['responsible_person'],
                            "responsible_person_name"=> $value['responsible_person_name'],
                            "responsible_person_mobile_number" => $value['responsible_person_mobile_number'],
                            "address"=> $value['address'],
                            "refered_by_doctor"=> $value['refered_by_doctor'],
                        'appointment_scheduled_details' => DB::table('appointment_scheduled_information')
                            ->select('users.first_name as doctor_first_name','users.last_name as doctor_last_name','appointment_scheduled_information.appointment_date_time','appointment_scheduled_information.appointment_status')
                            ->leftJoin('patient_information', 'patient_information.id', '=', 'appointment_scheduled_information.patient_id')
                            ->leftJoin('users', 'users.id', '=', 'appointment_scheduled_information.doctor_id')
                            ->where('appointment_scheduled_information.patient_id',$value['id'])
                            ->orderBy('appointment_date_time','asc')
                            ->get(),
                        ];
                    }
                }
            sort($completed_patient_details_array);
            return response()->json([
                'data' =>$completed_patient_details_array ,
                'success' => true,
            ], 200);
        }
    }
 //display date wise patient information   
    public function getDateWisePatientDetails(Request $request){
        if (Auth::check()){
            $request->validate([
                'date_wise' => 'required|date|date_format:Y-m-d',
            ]);
            $patient_details = DB::table('appointment_scheduled_information')
                ->select('patient_information.id','patient_information.patient_id','patient_information.patient_name', 'patient_information.birth_date','patient_information.gender',
                        'patient_information.marital_status','patient_information.age','patient_information.occupation','patient_information.mobile_number','patient_information.email','patient_information.responsible_person_mobile_number',
                        'patient_information.language','patient_information.nationality','patient_information.responsible_person','patient_information.responsible_person_name','patient_information.address','patient_information.refered_by_doctor',
                        'appointment_scheduled_information.appointment_date_time')
                ->leftJoin('patient_information', 'patient_information.id', '=', 'appointment_scheduled_information.patient_id')
                ->leftJoin('users','users.id','=','patient_information.created_by')
                ->leftJoin('roles','roles.id','=','users.role_id')
                ->where('roles.role','=','receptionist')
                ->where('appointment_date_time',$request->date_wise)
                ->get();
            $patient_details = json_decode($patient_details,true);
            return response()->json([
                'data' =>$patient_details ,
                'success' => true,
            ], 200);
            $completed_patient_details_array = array();
            if(!empty($patient_details) && is_array($patient_details) && sizeof($patient_details)){
                foreach ($patient_details as $key => $value) { 
                    $completed_patient_details_array[] = [
                            "id" => $value['id'],
                            "patient_id" => $value['patient_id'],
                            "patient_name" => $value['patient_name'],
                            "birth_date"=> $value['birth_date'],
                            "gender"=> $value['gender'],
                            "marital_status"=> $value['marital_status'],
                            "age"=> $value['age'],
                            "occupation"=> $value['occupation'],
                            "mobile_number"=> $value['mobile_number'],
                            "email"=> $value['email'],
                            "language"=> $value['language'],
                            "nationality"=> $value['nationality'],
                            "responsible_person"=> $value['responsible_person'],
                            "responsible_person_name"=> $value['responsible_person_name'],
                            "responsible_person_mobile_number" => $value['responsible_person_mobile_number'],
                            "address"=> $value['address'],
                            "refered_by_doctor"=> $value['refered_by_doctor'],
                        'appointment_scheduled_details' => DB::table('appointment_scheduled_information')
                            ->select('users.first_name as doctor_first_name','users.last_name as doctor_last_name','appointment_scheduled_information.appointment_date_time','appointment_scheduled_information.appointment_status')
                            ->leftJoin('patient_information', 'patient_information.id', '=', 'appointment_scheduled_information.patient_id')
                            ->leftJoin('users', 'users.id', '=', 'appointment_scheduled_information.doctor_id')
                            ->where('appointment_scheduled_information.patient_id',$value['id'])
                            ->orderBy('appointment_date_time','asc')
                            ->get(),
                        ];
                    }
                }
            sort($completed_patient_details_array);
            return response()->json([
                'data' =>$completed_patient_details_array ,
                'success' => true,
            ], 200);
        }
    }
    public function deletePatientDetails(Request $request){
        if(Auth::check()){
            if($request->route('id')){
                $patient_information_check_result = DB::table('patient_information')
                    ->select('patient_information.id')
                    ->where('patient_information.id',$request->route('id'))
                    ->get();
                $patient_information_check_result = json_decode($patient_information_check_result);
                if(!empty($patient_information_check_result) && is_array($patient_information_check_result) && sizeof($patient_information_check_result)){
                    DB::beginTransaction();
                    $deleted_patient_affected = DB::table('patient_information')
                            ->where('id',$request->route('id'))
                            ->delete();
                    if($deleted_patient_affected){
                         $appointment_scheduled_check_result = DB::table('appointment_scheduled_information')
                            ->select('appointment_scheduled_information.id')
                            ->where('appointment_scheduled_information.patient_id',$request->route('id'))
                            ->get();
                        $appointment_scheduled_check_result = json_decode($appointment_scheduled_check_result);
                        if(!empty($appointment_scheduled_check_result) && is_array($appointment_scheduled_check_result) && sizeof($appointment_scheduled_check_result)){
                            DB::beginTransaction();
                            $deleted_appointment_scheduled_affected = DB::table('appointment_scheduled_information')
                                    ->where('patient_id',$request->route('id'))
                                    ->delete();
                            if($deleted_appointment_scheduled_affected){
                                DB::commit();
                                return response()->json([
                                    'message' => 'Patient details deleted',
                                    'success' => true,
                                ], 200);
                            }
                        }else{
                            return response()->json([
                                'message' => 'Something went wrong',
                                'success' => false,
                            ], 200);
                        }
                        return response()->json([
                            'message' => 'Something went wrong',
                            'success' => false,
                        ], 200);
                    }else{
                        return response()->json([
                            'message' => 'Something went wrong',
                            'success' => false,
                        ], 404);
                    }
                }else{
                    return response()->json([
                        'message' => 'Patient not found',
                        'success' => false,
                    ], 404);
                }
            }else{
                return response()->json([
                    'message' => 'Patient ID is missing',
                    'success' => false,
                ], 400);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
}   

// $check_data= DB::table('patient_information')
//                ->select('patient_information.id','patient_information.patient_name','patient_information.birth_date','patient_information.mobile_number')
//                ->Where('patient_information.patient_name',$request->patient_name)
//                ->orWhere('patient_information.birth_date',$request->birth_date)
//                ->orWhere('patient_information.mobile_number',$request->mobile_number)
//                ->get();
//            $check_data = json_decode($check_data,true);
//            if (!empty($check_data) && is_array($check_data) && sizeof($check_data) > 0){
//                      return response()->json([
//                        'message' => 'Patient already exists',
//                        'success' => false,
//                    ], 200);
//            }else{


//$doctor_result = DB::table('appointment_scheduled_information')
//                    ->where('doctor_id',$request->doctor_id)
//                    ->where('appointment_date_time',$request->appointment_date_time)
//                    ->get();
//                $doctor_result = json_decode($doctor_result,true);
//                if(!empty($doctor_result) && is_array($doctor_result) && sizeof($doctor_result) > 0){
//                    return response()->json([
//                        'message' => 'Doctor has already appointment with this timings',
//                        'success' => true,
//                    ], 200);
//                }else{
//                 $appointment_schedule_updated_details = DB::table('appointment_scheduled_information')
//                        ->where('patient_id',$request->id)
//                        ->update([
//                            'doctor_id'=>$request->doctor_id,
//                            'appointment_date_time'=>$request->appointment_date_time,
//                            'appointment_status'=>'active',
//                            'scheduled_by' => Auth::user()->id,
//                            'updated_at' => Carbon::now()
//                    ]);
//                    if($appointment_schedule_updated_details){  