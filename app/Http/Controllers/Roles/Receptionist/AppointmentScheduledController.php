<?php

namespace App\Http\Controllers\Roles\Receptionist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Model\Appointment\AppointmentSchedule;

class AppointmentScheduledController extends Controller
{
    public function getDoctorNames(Request $request){
        if (Auth::check()){
            if($request->route('id')){
                $doctor_details = DB::table('users')
                    ->select('users.id','users.user_name','users.email','users.first_name','users.last_name')
                    ->leftJoin('roles','roles.id','=','users.role_id')
                    ->leftJoin('hospitals','hospitals.id','=','users.hospital_id')
                    ->where('users.active_status',1)
                    ->where('users.admin_status',1)
                    ->where('roles.role','=','doctor')
                    ->where('users.hospital_id',$request->route('id'))
                    ->get(); 
                return response()->json([
                    'message' => $doctor_details,
                    'success' => true,
                ], 200);
            }
        }else{
             return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
    
     public function getPatientNames(Request $request){
        if (Auth::check()){
            $request->validate([
                'search_name' => 'required|string|max:255',
            ]);
                $patient_details = DB::table('appointment_scheduled_information')
                    ->leftJoin('patient_information','patient_information.id','=','appointment_scheduled_information.patient_id')
                    ->select('patient_information.id','patient_information.patient_name','patient_information.patient_id')
                    ->where('patient_information.patient_name','LIKE','%'.$request->search_name.'%')
                    ->orWhere('patient_information.patient_id','LIKE','%'.$request->search_name.'%')
                    ->whereDate('appointment_scheduled_information.appointment_date_time' , '<' , Carbon::now())
                    ->get(); 
                return response()->json([
                    'message' => $patient_details,
                    'success' => true,
                ], 200);
        }else{
             return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
    
    public function scheduleAppointment(Request $request){
        if(Auth::check()){
            $request->validate([
                'doctor_id' => 'required|max:255|exists:users,id|integer',
                'patient_id' => 'required|max:255|exists:patient_information,id|integer',
                'appointment_date_time'=>'required|date|date_format:Y-m-d H:i',
            ]);
            DB::beginTransaction();
            $doctor_result = DB::table('appointment_scheduled_information')
                    ->where('doctor_id',$request->doctor_id)
                    ->where('appointment_date_time',$request->appointment_date_time)
                    ->get();
            $doctor_result = json_decode($doctor_result,true);
            if(!empty($doctor_result) && is_array($doctor_result) && sizeof($doctor_result) > 0){
                return response()->json([
                    'message' => 'Doctor has already appointment with this timings',
                    'success' => true,
                ], 200);
            }else{
                $appstartId = 1000;
                $appointment_schedule_details = new AppointmentSchedule([
                    'doctor_id'=>$request->doctor_id,
                    'patient_id'=>$request->patient_id,
                    'appointment_date_time'=>$request->appointment_date_time,
                    'appointment_status'=>'active',
                    'scheduled_by'=>Auth::user()->id
                ]);
                if($appointment_schedule_details->save()){
                    if($appointment_schedule_details->id){
                        DB::table('appointment_scheduled_information')->where('id',$appointment_schedule_details->id)->update(['appointment_id'=> ($appstartId + $appointment_schedule_details->id),]);
                        DB::commit();
                        return response()->json([
                            'message' => 'Appointment scheduled successfully',
                            'success' => true,
                        ], 200);
                    }
                }
            }
        }
    }
     public function getScheduledAppointmentsList(Request $request){
        if(Auth::check()){
            $appointment_doctor_list = DB::table('appointment_scheduled_information')
                    ->select('users.first_name as doctor_first_name','users.last_name as doctor_last_name','appointment_scheduled_information.id','patient_information.patient_name','patient_information.mobile_number',
                            'patient_information.birth_date','appointment_scheduled_information.appointment_date_time','appointment_scheduled_information.appointment_status','appointment_scheduled_information.appointment_id','patient_information.patient_id')
                    ->leftJoin('patient_information', 'patient_information.id', '=', 'appointment_scheduled_information.patient_id')
                    ->leftJoin('users', 'users.id', '=', 'appointment_scheduled_information.doctor_id')
                    ->orderBy('appointment_date_time','asc')
                    ->get();
            return response()->json([
                'message' => $appointment_doctor_list,
                'success' => true,
            ], 200);
        }
    }
    
     public function updateScheduledAppointmentDetails(Request $request){
        if(Auth::check()){
             $request->validate([
                'id' => 'required|integer|exists:appointment_scheduled_information,id',
                'doctor_id' => 'required|max:255|exists:users,id|integer',
                'appointment_date_time'=>'required|date|date_format:Y-m-d H:i',

            ]);
             DB::beginTransaction();
            $doctor_result = DB::table('appointment_scheduled_information')
                    ->where('doctor_id',$request->doctor_id)
                    ->where('appointment_date_time',$request->appointment_date_time)
                    ->get();
            $doctor_result = json_decode($doctor_result,true);
            if(!empty($doctor_result) && is_array($doctor_result) && sizeof($doctor_result) > 0){
                return response()->json([
                    'message' => 'Doctor has already appointment with this timings',
                    'success' => true,
                ], 200);
            }else{
             $patient_updated_result = DB::table('appointment_scheduled_information')
                 ->where('id',$request->id)
                  ->update([
                        'doctor_id'=>$request->doctor_id,
                        'appointment_date_time'=>$request->appointment_date_time,
                        'appointment_status'=>'active',
                        'scheduled_by' => Auth::user()->id,
                        'updated_at' => Carbon::now()
                ]);
              if($patient_updated_result){  
                 DB::commit();
                  return response()->json([
                      'message' => 'Appointment details updated',
                       'success' => true,
                  ], 200);
              }
            }
        }
    }
    
    public function deleteScheduledAppointmentDetails(Request $request){
        if(Auth::check()){
            if($request->route('id')){
                $appointment_scheduled_check_result = DB::table('appointment_scheduled_information')
                    ->select('appointment_scheduled_information.id')
                    ->where('appointment_scheduled_information.id',$request->route('id'))
                    ->get();
                $appointment_scheduled_check_result = json_decode($appointment_scheduled_check_result);
                if(!empty($appointment_scheduled_check_result) && is_array($appointment_scheduled_check_result) && sizeof($appointment_scheduled_check_result)){
                    DB::beginTransaction();
                    $deleted_appointment_scheduled_affected = DB::table('appointment_scheduled_information')
                            ->where('id',$request->route('id'))
                            ->delete();
                    if($deleted_appointment_scheduled_affected){
                        DB::commit();
                        return response()->json([
                            'message' => 'Appointment details deleted',
                            'success' => true,
                        ], 200);
                    }
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => false,
                    ], 200);
                }else{
                    return response()->json([
                        'message' => 'Appointment not found',
                        'success' => false,
                    ], 404);
                }
            }else{
                return response()->json([
                    'message' => 'Appointment ID is missing',
                    'success' => false,
                ], 400);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
}
