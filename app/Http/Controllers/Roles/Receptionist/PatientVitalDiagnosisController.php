<?php

namespace App\Http\Controllers\Roles\Receptionist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Receptionist\Vitals;
use App\Model\Receptionist\Diagnosis;
use App\Model\Receptionist\PatientDocuments;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PatientVitalDiagnosisController extends Controller
{
    //create patient vitals details    
    public function addPatientVitalsDetails(Request $request) {
        if (Auth::check()){
            $request->validate([
                'id' => 'required|integer|exists:patient_information,id',
                'height' => 'string|max:255',
                'weight' => 'string|max:255',
                'temperature' => 'string|max:255',
                'respiratory_rate'=>'string|max:255',
                'blood_pressure' => 'string|max:255',        
                'blood_oxygen_saturation'=>'string|max:255'
            ]);
            DB::beginTransaction();
            $check_vitals_result = DB::table('vitals')->where('patient_id',$request->id)->get();
            $check_vitals_result = json_decode($check_vitals_result,true);
            if(!empty($check_vitals_result) && is_array($check_vitals_result) && sizeof($check_vitals_result) > 0){
                $vitals_details_updated_result = DB::table('vitals')->where('patient_id',$request->id)->update([
                    'height' => $request->height,
                    'weight' => $request->weight,
                    'temperature' => $request->temperature,
                    'respiratory_rate' => $request->respiratory_rate,
                    'blood_pressure' => $request->blood_pressure,
                    'blood_oxygen_saturation' => $request->blood_oxygen_saturation,
                    'updated_at' => Carbon::now()]);
                if($vitals_details_updated_result){
                    DB::commit();
                    return response()->json([
                        'message' => 'Vitals details updated successfully',
                        'success' => true,
                    ], 200);
                }
            }else{
                DB::beginTransaction();
                $vital_insert_result = new Vitals([
                    'patient_id' => $request->id,
                    'height' => $request->height,
                    'weight' => $request->weight,
                    'temperature' => $request->temperature,
                    'respiratory_rate' => $request->respiratory_rate,
                    'blood_pressure' => $request->blood_pressure,
                    'blood_oxygen_saturation' => $request->blood_oxygen_saturation,
                ]);
                $vital_insert_result->save();
                DB::commit();
                return response()->json([
                    'message' => 'Vitals details created seccessfully',
                    'success' => true,
                ], 200);
            }
        }
    }
//create patient diagnosis details    
    public function addPatientDiagnosisDetails(Request $request){
        if (Auth::check()){
            $request->validate([
                'id' => 'required|integer|exists:patient_information,id',
                'diagnosis_name' => 'string|max:255',
                'diagnosis_type' => 'string|max:255',
            ]);
            DB::beginTransaction();
            $check_diagnosis_result = DB::table('diagnosis')->where('patient_id',$request->id)->get();
            if($check_diagnosis_result){
                $diagnosis_details_updated_result = DB::table('diagnosis')->where('patient_id',$request->id)->update([
                    'diagnosis_name' => $request->diagnosis_name,
                    'diagnosis_type' => $request->diagnosis_type,
                    'updated_at' => Carbon::now()]);
                if($diagnosis_details_updated_result){
                     DB::commit();
                    return response()->json([
                        'message' => 'Diagnosis details updated successfully',
                        'success' => true,
                    ], 200);
                }
            }
            DB::beginTransaction();
            $diagnosis_insert_result = new Diagnosis([
                'patient_id' => $request->id,
                'diagnosis_name' => $request->diagnosis_name,
                'diagnosis_type' => $request->diagnosis_type,
            ]);
            $diagnosis_insert_result->save();
            DB::commit();
            return response()->json([
                'message' => 'Diagnosis details created seccessfully',
                'success' => true,
            ], 200);
        }
    }    
//file upload    
    public function fileUpload(Request $request){
        if (Auth::check()){
            if($request->file('image_file') && $request->file('image_file')->isValid()){
                $request->validate([
                    'id' => 'required|integer|exists:patient_information,id',
                    'image_file' => 'required|mimes:jpeg,png,jpg,doc,pdf,docx',
                ]);
                $file = $request->file('image_file');
                if($file){
                    $image_name = 'files'.time().'.'.$file->getClientOriginalExtension();
                    $image_type = $file->getMimeType();
                    $original_file_name = $file->getClientOriginalName();
                    $destinationPath = public_path('/files');
                    if(!is_dir($destinationPath)){
                        mkdir($destinationPath, 0777, true);
                    }
                    $file->move($destinationPath, $image_name);
                    $images_details = new PatientDocuments([
                        'file_path'=>'files/'.$image_name,
                        'file_type'=>$image_type,
                        'original_file_name'=>$original_file_name,
                        'patient_id' => $request->id,
                    ]);
                    if($images_details->save()){
                        return response()->json([
                            'message' => 'File uploaded successfully',
                            'file_id'=>$images_details->id,
                            'success' => true,
                        ], 200);
                    }
                }else{
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => false,
                    ], 400);
                }
            }else{
                return response()->json([
                    'message' => 'No File chosen',
                    'success' => false,
                ], 400);
            }
        }
    }
    
    public function fileDownload(Request $request){
        if($request->route('id')){
            $file_details = PatientDocuments::where('id',$request->route('id'))->get();
            if(!empty($file_details) && sizeof($file_details)>0){
                if(sizeof($file_details) == 1){
                    $file_path='';$headers = ['Content-Type: image/jpeg'];$file_new_name = 'file-'.time().'.jpg';
                    foreach($file_details as $value) {
                        $file_path = public_path((!empty($value['file_path']) ? $value['file_path'] : null));
                        $headers = ['Content-Type: '.(!empty($value['file_type']) ? $value['file_type'] : 'image/jpeg').''];
                        $file_new_name = (!empty($value['original_file_name']) ? $value['original_file_name'] : 'file-'.time().'.jpg');
                        if(!file_exists($file_path)){
                            abort(404);
                        }
                    }
                    if($file_path){
                        return response()->download($file_path, $file_new_name, $headers);
                    }else{
                        abort(404);
                    }
                }else{
                    abort(404);
                }
            }else{
               abort(404);
            }
        }else{
            abort(404);
        }
    }
}
