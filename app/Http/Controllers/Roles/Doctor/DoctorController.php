<?php

namespace App\Http\Controllers\Roles\Doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DoctorController extends Controller
{
    //get appointment scheduled details w.r.to doctor wise
    public function getScheduledAppointmentsDetailsDoctorWise(Request $request){
        if (Auth::check()){
            $patient_details = DB::table('appointment_scheduled_information')
                   ->select('appointment_scheduled_information.patient_id','appointment_scheduled_information.doctor_id','patient_information.patient_name', 'patient_information.birth_date','patient_information.gender',
                          'patient_information.marital_status','patient_information.age','patient_information.occupation','patient_information.mobile_number','patient_information.email',
                          'patient_information.language','patient_information.nationality','patient_information.responsible_person','patient_information.responsible_person_name','patient_information.address','patient_information.refered_by_doctor')
                   ->leftJoin('patient_information', 'patient_information.id', '=', 'appointment_scheduled_information.patient_id')    
                   ->leftJoin('users','users.id','=','patient_information.created_by')
                   ->leftJoin('roles','roles.id','=','users.role_id')
                   ->where('appointment_scheduled_information.doctor_id',Auth::user()->id)
                   ->get();
            $patient_details = json_decode($patient_details,true);
            $completed_patient_details_array = array();
            if(!empty($patient_details) && is_array($patient_details) && sizeof($patient_details)){
                foreach ($patient_details as $key => $value) { 
                     $completed_patient_details_array[] = [
                                "patient_name" => $value['patient_name'],
                                "birth_date"=> $value['birth_date'],
                                "gender"=> $value['gender'],
                                "age"=> $value['age'],
                                "mobile_number"=> $value['mobile_number'],
                                "email"=> $value['email'],
                                "responsible_person"=> $value['responsible_person'],
                                "responsible_person_name"=> $value['responsible_person_name'],
                                "address"=> $value['address'],
                                "refered_by_doctor"=> $value['refered_by_doctor'],
                            'appointment_scheduled_details' => DB::table('appointment_scheduled_information')
                                ->select('appointment_scheduled_information.id','appointment_scheduled_information.appointment_date_time','appointment_scheduled_information.appointment_status')
                                ->leftJoin('patient_information', 'patient_information.id', '=', 'appointment_scheduled_information.patient_id')
                                ->where('appointment_scheduled_information.doctor_id',Auth::user()->id)
                                ->orderBy('appointment_date_time','asc')
                                ->get(),
                            'lab_report_details' => DB::table('lab_reports')
                                ->select('lab_reports.id','lab_reports.lab_report','lab_reports.lab_reporter_id','lab_reports.lab_report_status')
                                ->leftJoin('appointment_scheduled_information','appointment_scheduled_information.id', '=', 'lab_reports.appointment_scheduled_id')
                                ->where('appointment_scheduled_information.patient_id', $value['patient_id'])
                                ->get(),
                            'pharmacy_report_details' => DB::table('pharmacy_reports')
                                ->select('pharmacy_reports.id','pharmacy_reports.pharmacy_reporter_id','pharmacy_reports.pharmacy_report','pharmacy_reports.pharmacy_report_status')
                                ->leftJoin('appointment_scheduled_information','appointment_scheduled_information.id', '=', 'pharmacy_reports.appointment_scheduled_id')
                                ->where('appointment_scheduled_information.patient_id', $value['patient_id'])
                                ->get(),
                        ];
                    }
                }
            sort($completed_patient_details_array);
            return response()->json([
                'data' =>$completed_patient_details_array ,
                'success' => true,
            ], 200);
        }
    }
    
    //get lab reporter/lab technician detials
    public function getLabReporterNames(Request $request){
        if (Auth::check()){
            if($request->route('id')){
                $lab_reporter_details = DB::table('users')
                    ->select('users.id','users.user_name','users.email','users.first_name','users.last_name')
                    ->leftJoin('roles','roles.id','=','users.role_id')
                    ->leftJoin('hospitals','hospitals.id','=','users.hospital_id')
                    ->where('users.active_status',1)
                    ->where('users.admin_status',1)
                    ->where('roles.role','=','lab_technologist')
                    ->where('users.hospital_id',$request->route('id'))
                    ->get(); 
                return response()->json([
                    'message' => $lab_reporter_details,
                    'success' => true,
                ], 200);
            }
        }else{
             return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
    
    //get pharmacy reporter /technician reporter details
    public function getPharmacyReporterNames(Request $request){
        if (Auth::check()){
            if($request->route('id')){
                $pharmacy_reporter_details = DB::table('users')
                    ->select('users.id','users.user_name','users.email','users.first_name','users.last_name')
                    ->leftJoin('roles','roles.id','=','users.role_id')
                    ->leftJoin('hospitals','hospitals.id','=','users.hospital_id')
                    ->where('users.active_status',1)
                    ->where('users.admin_status',1)
                    ->where('roles.role','=','pharmacy_technician')
                    ->where('users.hospital_id',$request->route('id'))
                    ->get(); 
                return response()->json([
                    'message' => $pharmacy_reporter_details,
                    'success' => true,
                ], 200);
            }
        }else{
             return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
  
    //get health care workers details
    public function getHelathCareWorkersNames(Request $request){
        if (Auth::check()){
            if($request->route('id')){
                $health_care_workers_details = DB::table('users')
                    ->select('users.id','users.user_name','users.email','users.first_name','users.last_name')
                    ->leftJoin('roles','roles.id','=','users.role_id')
                    ->leftJoin('hospitals','hospitals.id','=','users.hospital_id')
                    ->where('users.active_status',1)
                    ->where('users.admin_status',1)
                    ->where('roles.role','=','health_care_workers')
                    ->where('users.hospital_id',$request->route('id'))
                    ->get(); 
                return response()->json([
                    'message' => $health_care_workers_details,
                    'success' => true,
                ], 200);
            }
        }else{
             return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
}
