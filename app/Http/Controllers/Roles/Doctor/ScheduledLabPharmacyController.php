<?php

namespace App\Http\Controllers\Roles\Doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\Doctor\LabReports;
use App\Model\Doctor\PharmacyReports;

class ScheduledLabPharmacyController extends Controller
{
    //doctor assign to lab report details
    public function scheduledLabReportDetails(Request $request){
        if (Auth::check()){
            $request->validate([
                'appointment_scheduled_id' => 'required|max:255|exists:appointment_scheduled_information,id|integer',
                'lab_reporter_id'=>'required|max:255|exists:users,id|integer',
                'lab_report' => 'required|string|max:255',
                'lab_report_scheduled_date_time'=>'required|date|date_format:Y-m-d H:i',
            ]);
            DB::beginTransaction();
            $lab_reporter_result = DB::table('lab_reports')
                   ->where('lab_reporter_id',$request->lab_reporter_id)
                   ->where('lab_report_scheduled_date_time',$request->lab_report_scheduled_date_time)
                   ->get();
            $lab_reporter_result = json_decode($lab_reporter_result,true);
            if(!empty($lab_reporter_result) && is_array($lab_reporter_result) && sizeof($lab_reporter_result) > 0){
               return response()->json([
                   'message' => 'Lab report already assign to this timings',
                   'success' => true,
               ], 200);
            }else{
               $patient_insert_result = new LabReports([
                    'appointment_scheduled_id' => $request->appointment_scheduled_id,
                    'lab_reporter_id' => $request->lab_reporter_id,
                    'lab_report' => $request->lab_report,
                    'lab_report_status' => 'active',
                    'lab_report_scheduled_date_time' => $request->lab_report_scheduled_date_time,
               ]);
                if ($patient_insert_result->save()){
                    DB::commit();
                    return response()->json([
                        'message' => 'Lab report assign successfully',
                        'success' => true,
                    ], 200);
                }else {
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => false,
                    ], 400);
                }
           }
        }
    }
    
    //doctor assign to phramacy report details
    public function scheduledPharmacyReportDetails(Request $request){
        if (Auth::check()){
            $request->validate([
                'appointment_scheduled_id' => 'required|max:255|exists:appointment_scheduled_information,id|integer',
                'pharmacy_reporter_id'=>'required|max:255|exists:users,id|integer',
                'pharmacy_report' => 'required|string|max:255',
                'pharmacy_report_scheduled_date_time'=>'required|date|date_format:Y-m-d H:i',
            ]);
            DB::beginTransaction();
            $pharmacy_reporter_result = DB::table('pharmacy_reports')
                   ->where('pharmacy_reporter_id',$request->pharmacy_reporter_id)
                   ->where('pharmacy_report_scheduled_date_time',$request->pharmacy_report_scheduled_date_time)
                   ->get();
            $pharmacy_reporter_result = json_decode($pharmacy_reporter_result,true);
            if(!empty($pharmacy_reporter_result) && is_array($pharmacy_reporter_result) && sizeof($pharmacy_reporter_result) > 0){
               return response()->json([
                   'message' => 'Pharmacy report already assign to this timings',
                   'success' => true,
               ], 200);
            }else{
                $patient_insert_result = new PharmacyReports([
                    'appointment_scheduled_id' => $request->appointment_scheduled_id,
                    'pharmacy_reporter_id' => $request->pharmacy_reporter_id,
                    'pharmacy_report' => $request->pharmacy_report,
                    'pharmacy_report_status' => 'active',
                    'pharmacy_report_scheduled_date_time' => $request->pharmacy_report_scheduled_date_time,
                 ]);
                if ($patient_insert_result->save()){
                    DB::commit();
                    return response()->json([
                         'message' => 'Pharmacy report assign successfully',
                         'success' => true,
                    ], 200);
                }else {
                     return response()->json([
                         'message' => 'Something went wrong',
                         'success' => false,
                     ], 400);
                
                }
            }
        }
    }
}
