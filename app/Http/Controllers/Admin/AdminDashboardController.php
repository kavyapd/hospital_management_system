<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Model\Register\Hospitals;
use Carbon\Carbon;

class AdminDashboardController extends Controller
{
//get hospital list for admin dashboard    
     public function gethospitalDetails(Request $request){
        if(Auth::check()){
            $hospital_list = DB::table('hospitals')
                    ->select('hospitals.id','hospital_name')
                    ->leftJoin('users','users.id','=','hospitals.user_id')
                    ->leftJoin('roles','roles.id','=','users.role_id')
                    ->where('roles.role','admin')
                    ->get();    
            return response()->json([
                 'data' => $hospital_list,
                 'success' => true,
             ], 200);
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
 //get user registered list for admin dashboard    
    public function getUserDetails(Request $request){
        if(Auth::check()){
            $user_details = DB::table('users')
                    ->select('users.id','users.user_name','users.email','users.first_name','users.last_name','roles.role_name','hospitals.hospital_name','users.admin_status as user_status')
                    ->leftjoin('roles','roles.id','=','users.role_id')
                    ->leftJoin('hospitals','hospitals.id','=','users.hospital_id')
                    ->where('roles.id','!=',1)
                    ->where('users.active_status',1)
                    ->get();
             return response()->json([
                'message' => $user_details,
                'success' => true,
            ], 200);
        }
    }
 //add hospital details admin dashboard    
    public function addHospitalDetails(Request $request){
        if(Auth::check()){
             $request->validate([
                 'hospital_name' => 'required|string|max:255|unique:hospitals'
             ]);
             $hospital_insert_result = new Hospitals([
                 'hospital_name' => $request->hospital_name,
                 'user_id' => DB::table('roles')->where('role','admin')->first()->id,
             ]);
             $hospital_insert_result->save();
             return response()->json([
                 'message' => 'Hospital added successfully',
                 'success' => true,
             ], 200);
        }
    }
    
    public function updateHospitalsDetails(Request $request){
        if(Auth::check()){
            $request->validate([
                'id' => array('required','integer','exists:hospitals,id',),
                'hospital_name' => 'required|string|max:255|unique:hospitals',
            ]);
            DB::beginTransaction();
            $hospital_updated_result = DB::table('hospitals')
                ->where('id',$request->id)
                ->update([
                    'hospital_name' => $request->hospital_name,
                    'updated_at' => Carbon::now()
                ]);
            if($hospital_updated_result){  
                 DB::commit();
                return response()->json([
                    'message' => 'Hospital name updated successfully',
                    'success' => true,
                ], 200);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 400);
        }
    }
//delete hospital details  
    public function deleteHospital(Request $request){
        if(Auth::check()){
            if($request->route('id')){
                $hospital_check_result = DB::table('hospitals')
                    ->select('hospitals.id')
                    ->where('hospitals.id',$request->route('id'))
                    ->get();
                $hospital_check_result = json_decode($hospital_check_result);
                if(!empty($hospital_check_result) && is_array($hospital_check_result) && sizeof($hospital_check_result)){
                    DB::beginTransaction();
                    $deleted_hospital_affected = DB::table('hospitals')
                            ->where('id',$request->route('id'))
                            ->delete();
                    if($deleted_hospital_affected){
                        DB::commit();
                        return response()->json([
                            'message' => 'Hospital deleted successfully',
                            'success' => true,
                        ], 200);
                    }
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => false,
                    ], 200);
                }else{
                    return response()->json([
                        'message' => 'Hospital not found',
                        'success' => false,
                    ], 404);
                }
            }else{
                return response()->json([
                    'message' => 'Hospital ID is missing',
                    'success' => false,
                ], 400);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
}