<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Mail;

class ApprovedRejectController extends Controller
{
//admin approved a user account    
    public function adminApprovalStatus(Request $request){
        if (Auth::check()){
            if ($request->route('id')){
                $admin_approved_status_check = DB::table('users')
                    ->select('users.email')
                    ->leftjoin('roles','roles.id','=','users.role_id')
                    ->where('users.id',$request->route('id'))
                    ->where('active_status',1)
                    ->where('roles.id','!=',1)
                    ->get();
               $admin_approved_status_check = json_decode($admin_approved_status_check,true);
               if ($admin_approved_status_check){
                   DB::beginTransaction();
                   $admin_approved_status = DB::table('users')
                       ->select('users.email')
                       ->where('users.id',$request->route('id'))
                       ->update(['admin_status'=>1,'updated_at' => Carbon::now()]);
                    if ($admin_approved_status){
                       DB::commit(); 
                       $link = array('link' => "http://192.168.0.175/HMS/#/login");
                       Mail::send('approveuser',$link, function($mail) use ($admin_approved_status_check) {
                           $mail->from('noreply@domytaxonline.com.au', 'Hospital Managament System')->to($admin_approved_status_check[0]['email'])->subject('Admin Approved Your Account');
                       });
                       if (Mail::failures()){
                           return response()->json([
                               'message' => 'Email could not be sent',
                               'success'=> false,
                           ],400);
                        }else{
                           DB::commit();
                           return response()->json([
                               'message' => 'User approved successfully',
                               'success' => true,
                           ], 200);
                        }
                    }else{
                      return response()->json([
                           'message' => 'Something went wrong2',
                           'success' => false,
                       ], 400); 
                    }
                }else{
                    return response()->json([
                            'message' => 'User not found',
                            'success' => false,
                        ], 400); 
                }
            }else{
                return response()->json([
                    'message' => 'User ID is missing',
                    'success' => false,
                ], 400);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
//admin reject a user account    
    public function adminRejectStatus(Request $request){
        if(Auth::check()){
            if($request->route('id')){
            $admin_approved_status_check = DB::table('users')
                ->select('users.admin_status','users.email')
                ->leftjoin('roles','roles.id','=','users.role_id')
                ->where('users.id',$request->route('id'))
                ->where('active_status',1)
                ->where('roles.id','!=',1)
                ->get();
            $admin_approved_status_check = json_decode($admin_approved_status_check,true);
                if ($admin_approved_status_check){
                    DB::beginTransaction();
                    $active_account = DB::table('users')
                        ->where('users.id',$request->route('id'))
                        ->update(['admin_status'=>2,'updated_at' => Carbon::now()]);
                    if ($active_account){
                        DB::commit(); 
                        $link = array('link' => "http://192.168.0.175/HMS/#/login");
                           Mail::send('adminreject',$link, function($mail) use ($admin_approved_status_check) {
                               $mail->from('noreply@domytaxonline.com.au', 'Hospital Managament System')->to($admin_approved_status_check[0]['email'])->subject('Admin Reject Your Account');
                           });
                           if (Mail::failures()){
                               return response()->json([
                                   'message' => 'Email could not be sent',
                                   'success'=> false,
                               ],400);
                           }else{
                               DB::commit();
                               return response()->json([
                                   'message' => 'User rejected successfully',
                                   'success' => true,
                               ], 200);
                           }
                    }else{
                       return response()->json([
                            'message' => 'Something went wrong',
                            'success' => false,
                        ], 200); 
                    }
                }else{
                    return response()->json([
                        'message' => 'User not found',
                        'success' => false,
                    ], 400); 
                }
            }else{
                return response()->json([
                    'message' => 'User ID is missing',
                    'success' => false,
                ], 400);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
}
