<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Register\PasswordResets;
use Mail;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;

class ResetPassword extends Controller {
    
    /**
    * Function to Reset password by 
    * verification mail process
    */
    public function requestResetPassword(Request $request){
        $request->validate([
            'email' => 'required|string|email|exists:users',
        ]);
        $account_verified_check_result = DB::table('users')
                ->select('users.email','users.id')
                ->where('email',$request->email)
                ->where('active_status',1)
                ->get();
        $account_verified_check_result = json_decode($account_verified_check_result,true);
        if (!empty($account_verified_check_result) && is_array($account_verified_check_result) && sizeof($account_verified_check_result)){
            DB::beginTransaction();
            $token = Uuid::uuid1()->toString();
            $auth = bcrypt($request->email);
            $link = array('link' => "http://192.168.0.133/HMS/#/reset-password?auth=$auth&token=$token");
            Mail::send('requestresetpassword', $link, function($mail) use ($request) {
                $mail->from('noreply@domytaxonline.com.au', 'Hospital Managament System')->to($request->email)->subject('Reset Your Password');
            });
            if (Mail::failures()){
                return response()->json([
                    'message' => 'Email could not be sent',
                    'success'=> false,
                ],400);
            }else{
                $request_reset_password = new PasswordResets([
                        'user_id' =>$account_verified_check_result[0]['id'],
                        'reset_password_token'=>$token,
                        'updated_at' => Carbon::now()
                    ]);
                if ($request_reset_password->save()){
                    DB::commit();
                    return response()->json([
                        'message' => "Successfully sent mail. Please check your mailbox",
                        'success'=>true,
                    ],200);
                }else{
                    return response()->json([
                        'message' => "Something went wrong",
                        'success'=>false,
                    ],400);
                }
            }
        }else{
            return response()->json([
                'message' => "Account not found",
                'success'=>false,
            ],400);
        }
    }
    
    /**
    * Function to Resetting password by 
    *  using token and auth
    */
    public function resetPassword(Request $request){
        $request->validate([
            'reset_password_token' => 'required|string|exists:password_resets',
            'auth_token'=>'required|string',
            'new_password' => 'required|string',
            'confirm_password' => 'required|string|same:new_password',
        ]);
       
        $email_result = DB::table('password_resets')
            ->select('users.email','password_resets.user_id')
            ->leftJoin('users','users.id','=','password_resets.user_id')
            ->where('password_resets.reset_password_token',$request->reset_password_token)
            ->get();
        $email_result = json_decode($email_result,true);
        if (!empty($email_result) && is_array($email_result) && sizeof($email_result) > 0){
            if (isset($email_result[0]['email']) && password_verify($email_result[0]['email'], $request->auth_token)){
                DB::beginTransaction();
                $reset_password = DB::table('password_resets')
                    ->select('users.email')
                    ->leftJoin('users','users.id','=','password_resets.user_id')
                    ->where('password_resets.reset_password_token',$request->reset_password_token)
                    ->where('users.email',$email_result[0]['email'])
                    ->update(['password_resets.reset_password_token'=>'', 'password_resets.updated_at' => Carbon::now()]);
                if ($reset_password){
                    $password_reset_result = DB::table('users')
                            ->where('id',$email_result[0]['user_id'])
                            ->where('active_status',1)
                            ->update(['password'=>bcrypt($request->confirm_password),'updated_at' => Carbon::now()]);
                    if ($password_reset_result){
                       DB::commit();
                       return response()->json([
                          'message' => "Your password has been reset successfully!",
                          'success'=>true,
                       ]);
                    }else{
                        return response()->json([
                            "message" => "Reset password could not be updated",
                            "success"=>false
                        ],400);
                    }
                }else{
                    return response()->json([
                        "message" => "Reset password could not be updated",
                        "success"=>false
                    ],400);
                }
            }
        }
        return response()->json([
            'message' => 'Something went wrong',
            'success' => false,
        ], 400);
    }
}