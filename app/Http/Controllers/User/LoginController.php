<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
//login credential   
    public function login(Request $request){
       $request->validate([
            'email_or_username' => 'required|string',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
       
        $field = filter_var($request->input('email_or_username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'user_name';
        $request->merge([$field => $request->input('email_or_username')]);
         if (!Auth::attempt($request->only($field, 'password')))
            return response()->json([
                'errors'=> [ 
                   'login_error'=>  ['Email or password is invalid'],
                 ],
            ], 401);
        
        $account_verified_status_result = DB::table('users')
                ->select('users.id','users.admin_status')
                ->where('id',$request->user()->id)
                ->where('active_status',1)
                ->get();
        $account_verified_status_result = json_decode($account_verified_status_result,true);
        if (!empty($account_verified_status_result) && is_array($account_verified_status_result) && sizeof($account_verified_status_result)){
            if (!empty($account_verified_status_result['0']['admin_status']) && $account_verified_status_result['0']['admin_status'] == 1){
                $user_details = DB::table('users')->select('users.id','users.user_name','users.email','users.mobile_number','users.role_id','roles.role','users.hospital_id')
                    ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                    ->where('users.id',$request->user()->id)
                    ->get();
                $user_details = json_decode($user_details, true);
                $user = $request->user();
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                if (!$request->remember_me)
                    $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();

                return response()->json([
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse(
                        $tokenResult->token->expires_at
                    )->toDateTimeString(),
                    'success'=>true,
                    'user'=>((!empty($user_details) && sizeof($user_details) > 0) ? $user_details[0] : [])
                ]);
            }else if (!empty($account_verified_status_result['0']['admin_status']) && $account_verified_status_result[0]['admin_status'] == 2){
                return response()->json([
                     'errors'=> [ 
                        'login_error'=>  ['Your account is rejected by the site administrator'],
                      ],
                    'success' => false,
                ],400);
            }else{
                return response()->json([
                     'errors'=> [ 
                        'login_error'=>  ['Your account is currently pending approval by the site administrator'
                            ],
                      ],
                    'success' => false,
                ],400);
            }
        }else{
            return response()->json([
                 'errors'=> [ 
                        'login_error'=>  ['Email or Mobile number not verified'],
                      ],
                'success' => false,
            ],400);
        }
    }
    
   public function logout(Request $request){
        if ($request && $request->user() && $request->user()->token()){
            $request->user()->token()->revoke();
            return response()->json([
                'message' => 'Successfully logged out',
                'success' => true,
            ]);
        }else{
            return response()->json([
                'message' => 'Something went wrong',
                'success' => false,
            ]);
        }
    }
}
//https://www.google.com/url?q=http://192.168.0.100/hms/public/api/auth/account-activation?auth%3D$2y$10$gcNJqE2uXb20myjay.RMteiUd8hr.WecpzK4C.E9Jc4Iv5G3662sG%26token%3D76f98a4e-eb2b-11e9-ad46-00270e253b97&source=gmail&ust=1570787951726000&usg=AFQjCNGtTOzlahA0tUfs3kYRSat88c8JHA