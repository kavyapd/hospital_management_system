<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class ChangePasswordController extends Controller
{
    /**
    * Function to Change Password with existing password
    * 
    */
    public function changePassword(Request $request){
        if (Auth::check()){
            $request->validate([
                'old_password' => 'required|string|max:255',
                'new_password'=> 'required|string|max:255|min:8',
                'confirm_new_password' => 'required|string|max:255|min:8|same:new_password',
            ]);
            
            if (password_verify($request->old_password, Auth::user()->password)){
                if ($request->new_password != $request->confirm_new_password){
                    return response()->json([
                        'message' => 'Both password should be match',
                        'success' => false,
                    ], 200);
                }
                if ($request->old_password == $request->new_password){
                    return response()->json([
                        'message' => 'New Password should not be same as old password',
                        'success' => false,
                    ], 200);
                }
                DB::beginTransaction();
                $password_update_status = DB::table('users')->where('id',Auth::user()->id)->update(['password'=>bcrypt($request->new_password),'updated_at'=>Carbon::now()]);
                if ($password_update_status){
                    DB::commit();
                    return response()->json([
                        'message' => 'Password changed Successfully',
                        'success' => true,
                    ], 200);
                }else{
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => false,
                    ], 200);
                }
            }else{
                return response()->json([
                    'message' => 'Old Password is Wrong',
                    'success' => false,
                ], 200);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
}
