<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class AccountActivationController extends Controller
{
    /**
    * Function to Activate Account by clicking on 
    * verification mail
    */
    public function activateAccount(Request $request){
        if ($request->input('token') && $request->input('auth')){
            $email_token_check_result = DB::table('email_verified')
                ->select('users.email','email_verified.user_id')
                ->where('email_verified_token',$request->input('token'))
                ->leftJoin('users','users.id','=','email_verified.user_id')
                ->get();
            $email_token_check_result = json_decode($email_token_check_result,true);
            if (!empty($email_token_check_result) && is_array($email_token_check_result) && sizeof($email_token_check_result) > 0){
                if (isset($email_token_check_result[0]['email']) && password_verify($email_token_check_result[0]['email'], $request->input('auth'))){
                    DB::beginTransaction();
                    $active_account = DB::table('email_verified')
                            ->where('email_verified_token',$request->input('token'))
                            ->where('email_verified_status',0)
                            ->update(['email_verified_status'=>1, 'email_verified_token'=>'','updated_at' => Carbon::now()]);
                    $active_account = json_decode($active_account,true);
                    if ($active_account){
                        $users_updated_result = DB::table('users')
                            ->where('id',$email_token_check_result[0]['user_id'])
                            ->where('active_status',0)
                            ->update(['active_status'=>1,'updated_at' => Carbon::now()]);
                        if ($users_updated_result){
                            DB::commit();
                           return Redirect::to('http://192.168.0.133/HMS/#/login');
                        }else{
                            return Redirect::to('http://192.168.0.133/account?msg="already-verified"');
                        }
                    }
                }else{
                   return Redirect::to('http://192.168.0.133/error-page?msg="not-found"');
                }
            }else{
                return Redirect::to('http://192.168.0.133/account?msg="activation-link-expired"');
            }
        }else{
            return Redirect::to('http://192.168.0.133/error?msg="not-found"');
        }
    }
}

//http://192.168.0.175/account?msg="activation"
