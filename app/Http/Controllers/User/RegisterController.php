<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Model\Register\EmailVerified;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Mail;

class RegisterController extends Controller
{  
    //roles registration
    public function registerDetails(Request $request){
        $request->validate([
            'user_name' => 'required|string|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'mobile_number'=>'required|min:10|max:10|unique:users',
            'mobile_otp'=>'required|integer',
            'hospital_id'=>'required|exists:hospitals,id|integer',
            'role_id'=>'required|exists:roles,id|integer',
            'password' =>'required|max:255'
        ]);
        $check_data= DB::table('users')
            ->select('users.id','users.user_name','users.role_id','users.mobile_number','roles.role','users.active_status')
            ->leftJoin('roles','roles.id','=','users.role_id')
            ->where('users.user_name',$request->user_name)
            ->where('users.email',$request->email)
            ->where('users.mobile_number',$request->mobile_number)
            ->get();
        $check_data = json_decode($check_data,true);
        if (!empty($check_data) && is_array($check_data) && sizeof($check_data)){
            if (!empty($check_data['0']['role_id']) && $check_data['0']['role_id'] == $request->role_id){
                  return response()->json([
                    'message' => 'User already exists',
                    'success' => false,
                ], 200);
            }else{
                 return response()->json([
                    'message' => 'Role id missing',
                    'success' => false,
                ], 200);
            }
        }else{
            $token = Uuid::uuid1()->toString();
            $auth = bcrypt($request->email);
            $check_mobile_otp= DB::table('mobile_verified')
                    ->select('mobile_verified.mobile_otp')
                    ->where('mobile_verified.mobile_number',$request->mobile_number)
                    ->where('mobile_verified.mobile_otp',$request->mobile_otp)
                    ->get();
            $check_mobile_otp = json_decode($check_mobile_otp,true);
            $mobile_otp = "1234";
            DB::beginTransaction();
            if (!empty($check_mobile_otp) && is_array($check_mobile_otp) && sizeof($check_mobile_otp)){
                $users_insert_result = new User([
                    'user_name' => $request->user_name,
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'mobile_number' => $request->mobile_number,
                    'role_id' => $request->role_id,
                    'hospital_id' => $request->hospital_id,
                ]);
                if ($users_insert_result->save()){
                    if ($user_id = $users_insert_result->id){
                        $users_updated_result = new EmailVerified([
                                'user_id' =>$users_insert_result->id,
                                'email_verified_status'=>0,
                                'email_verified_token'=>$token,
                                'updated_at' => Carbon::now()
                            ]);
                        if ($users_updated_result->save()){
                            $link = array('link' => "http://192.168.0.199/hms/public/api/auth/account-activation?auth=$auth&token=$token");
                            Mail::send('verifyuser', $link, function($mail) use ($request) {
                                $mail->from('noreply@domytaxonline.com.au', 'Hospital Managament System')->to($request->email)->subject('Active Your Account');
                            });
                            if (Mail::failures()){
                                return response()->json([
                                    'message' => 'Email could not be sent',
                                    'success'=> false,
                                ],400);
                            }else{
                                DB::commit();
                                return response()->json([
                                    'message' => 'Mail sent successfully, Please verify the email address to activate your account',
                                    'success' => true,
                                ], 200);
                            }
                        }else{
                           return response()->json([
                                    'message' => 'Something went wrong',
                                    'success' => true,
                                ], 200); 
                        }
                    }else{
                        return response()->json([
                                    'message' => 'Something went wrong',
                                    'success' => true,
                                ], 200); 
                        }
                }else{
                    return response()->json([
                                'message' => 'Something went wrong',
                                'success' => true,
                            ], 200); 
                    }   
            }else{
                return response()->json([
                    'message' => 'Invalid OTP',
                    'errors'=> [ 
                                 'mobile_otp'=> ['Invalid OTP'],
                              ],
                    'success' => false,
                ], 400);
            }
        }
    }
//hospital and role names          
    public function getHospitalandRolesNames(){
        $roles_details = DB::table('roles')
                ->where('id','!=',1)
                ->get();
        $hospital_names = DB::table('hospitals')->select('id','hospital_name')->orderBy('hospital_name','asc')->get();
            return response()->json([
                'hospital_names' => $hospital_names,
                'role_name' => $roles_details,
                'success' => true,
            ], 200);
    } 
}
       
    
//    if(!empty($check_data['0']['user_name'])){
//                    if(!empty($check_data['0']['mobile_number'])){
//                        return response()->json([
//                                'message' => 'user already added',
//                                'success' => false,
//                            ], 200);
//                    }else {
//                    
//                    }
//                }else{
//                    
//                }