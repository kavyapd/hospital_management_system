<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\Register\MobileVerified;
use App\MSG91;


class SMSController extends Controller
{
//verify mobile number using mobile OTP    
    public function sendSMS(Request $request){
        $request->validate([
            'mobile_number'=>'required|numeric|digits:10|not_in:0',
        ]);
            DB::beginTransaction();
            $users_insert_result = new MobileVerified([
                'mobile_number' => $request->mobile_number,
                'mobile_otp' => '1234',
            ]);
          if($users_insert_result->save()){
          DB::commit();
          return response()->json([
                'message' => 'OTP send to your registered mobile number',
                'success' => true,
            ], 200);
          }
        }
        
        
    public function sendOtp(Request $request){
         $request->validate([
            'mobile_number'=>'required|numeric|digits:10|not_in:0',
        ]);
        $otp = rand(100000, 999999);
        $msg91Response = new MSG91();
        $msg91ResponseResult = $msg91Response->sendSMS($otp, $request->mobile_number);
        if(!empty($msg91ResponseResult) && is_array($msg91ResponseResult) && sizeof($msg91ResponseResult) > 0){
            DB::beginTransaction();
            $otp_save  = new MobileVerified([
                'mobile_number' => $request->mobile_number,
                'mobile_otp'=>$otp,
            ]);;
            if($otp_save->save()){
                DB::commit();
                return response()->json([
                    'message'=>"OTP send to your registered mobile number",
                    'success'=>true
                ],200);
            }
        }
    }
}
