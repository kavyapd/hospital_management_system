<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CustomClass\CustomValueBinder;

use Maatwebsite\Excel\Facades\Excel;

class SampleController extends Controller
{
    public function samplecode(Request $request){
                $request->validate([
                    'excel_file' => 'required|max:10240',
                ]);
               $file = $request->file('excel_file');
                        if($file){
                            $path = $file->getRealPath();
                            $myValueBinder = new CustomValueBinder();
                            $data = Excel::setValueBinder($myValueBinder)->load($path, function($reader) {})->noHeading()->formatDates(false)->get();
                            $patient_information_details = array();
                            $guarantor_information = array();
                            $insurance_information = array();
                            $encounter_inforamation = array();
                            if(!empty($data) && $data->count()){
                                foreach ($data as $key => $value){
                                    if(!empty($value[0]))
                                        $patient_information_details[0][trim($value[0])] = trim($value[1]);
                                      }

                                $patient_id = 0;
                                if(!empty($patient_information_details) && is_array($patient_information_details) && sizeof($patient_information_details) > 0){
                                    foreach ($patient_information_details as $key => $value) {
                                        DB::beginTransaction();
                                        $patient_information_details = ['Name'=>isset($value['Name']) ? $value['Name'] : ''];
                                        return response()->json([
                                            'message' => $patient_information_save_details,
                                            'success' => true,
                                        ], 200);
                                        
                                    }
                                }
                            }
                        }
    }
}
