<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
class CheckPatient implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value){
            $patient_check_result = DB::table('patient_information')
                ->select('patient_information.id')
                ->where('patient_information.id',$value)
                ->get();
             $patient_check_result = json_decode($patient_check_result);
            if(!empty($patient_check_result) && is_array($patient_check_result) && sizeof($patient_check_result)){
                return true;
            }else{
                return false;
            }
        }
           
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'User Not found';
    }
}
