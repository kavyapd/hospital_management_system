<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
class CheckRole implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value){
            $role_check_result = DB::table('users')
                ->select('users.id')
                ->leftJoin('roles','roles.id','=','users.role_id')
                ->where('users.id',$value)
                ->where('roles.id','!=',1)
                ->get();
             $role_check_result = json_decode($role_check_result);
            if(!empty($role_check_result) && is_array($role_check_result) && sizeof($role_check_result)){
                return true;
            }else{
                return false;
            }
        }
           
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'User Not found';
    }
}
