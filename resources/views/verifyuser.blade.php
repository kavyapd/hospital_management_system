<?php
echo '<div class="email_summary" style="mso-line-height-rule: exactly; mso-hide: all; display: none !important; font-size: 0 !important; max-height: 0 !important; line-height: 0 !important; padding: 0 !important; overflow: hidden !important; float: none !important; width: 0 !important; height: 0 !important;">HMS</div>
<table class="email_body" style="border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0; padding: 0;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td class="email_body_cell" style="padding: 16px 8px; text-align: center; vertical-align: top; font-size: 0; background-color: #ffffff;"><center>
                <div class="container_outer" style="vertical-align: top; max-width: 584px; margin: 0 auto;">
                    <table class="container" style="border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0 auto; vertical-align: top; -webkit-border-radius: 4px; border-radius: 4px; overflow: hidden; background-color: #fff;border:1px solid #ccc;"  width="100%" cellspacing="0" cellpadding="0" align="center">
                        <tbody>
                            <tr>
                                <td class="container_cell" style="padding: 0;" align="center" valign="top">
                                    <table class="jumbotron_native" style="border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td class="jumbotron_native_cell" style="padding: 10px; vertical-align: middle; text-align: center; background: #00B4DB; background: -webkit-linear-gradient(to bottom, #0083B0, #00B4DB); background: linear-gradient(to bottom, #0083B0, #00B4DB);">
													<h3 style="font-family: Helvetica, Arial, sans-serif; padding: 0px; font-weight: normal; font-size: 19px; line-height: 38px; color: #ffffff; margin: 10px 0px; text-align: left;"><span style="font-family: georgia, palatino, serif;"><strong>Account Activation</strong></span></h3>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="content content_article" style="border-spacing: 0px; height: 44px; width: 100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr style="height: 44px;">
                                                <td class="content_cell" style="padding: 24px 16px; vertical-align: top; font-size: 20px; text-align: center; border-bottom: 1px solid #ebebeb; height: 44px;">
                                                    <div class="two_thirds" style="display: inline-block; width: 100%; vertical-align: top; text-align: left; max-width: 550px;">
                                                        <table class="column" style="border-spacing: 0px; vertical-align: top; height: 80px; width: 100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody style="font-size:14px;">
                                                                <tr style="height: 16px;">
                                                                    <td class="column_cell font_default" style="padding: 8px 16px; font-family: Helvetica, Arial, sans-serif; vertical-align: top; text-align: left; height: 16px;">Please click the below link to verify the account.</td>
                                                                </tr>
                                                                <tr style="height: 16px;" align="center">
                                                                    <td style="padding: 8px 16px; font-family: Helvetica, Arial, sans-serif; vertical-align: top; text-align: left; height: 16px;">
                                                                    <a href="'.$link.'" style="text-decoration:none;border:none;border-radius:4px;padding:10px 10px;color:#fff;background: #00B4DB; background: -webkit-linear-gradient(to bottom, #0083B0, #00B4DB); background: linear-gradient(to bottom, #0083B0, #00B4DB);border-color:#17a2b8;">
                                                                    Verify Account
                                                                    </a>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 16px;">
                                                                    <td class="column_cell font_default" style="padding: 8px 16px; font-family: Helvetica, Arial, sans-serif; vertical-align: top; text-align: left; height: 16px;"></td>
                                                                </tr>
                                                                <tr style="height: 48px;">
                                                                    <td style="padding: 8px 16px; font-family: Helvetica, Arial, sans-serif; vertical-align: top; text-align: left; height: 48px;"><br />Sincerely,<br /><a href="https://exdionsg.app/HMS/" style="text-decoration:none;font-size:14px:color:#000;">Hospital Management System</a></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </center>
            </td>
        </tr>
    </tbody>
</table>';