<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();
        DB::table('users')->insert([
            'user_name' => 'admin',
            'first_name' => 'Kavya',
            'last_name' => 'P',
            'mobile_number' => '7022116068',
            'email' => 'admin@hms.com',
            'password' => bcrypt('admin@hms.com'),
            'role_id' => DB::table('roles')->where('role','admin')->first()->id,
            'hospital_id' => 1,
            'active_status' => 1,
            'admin_status' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
