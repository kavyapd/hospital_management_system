<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('roles')->truncate();
        
        $roles = [
            ['role' => 'admin','role_name' => 'Admin'],
            ['role' => 'receptionist','role_name' => 'Receptionist'], 
            ['role' => 'doctor','role_name' => 'Doctor'],
            ['role' => 'lab_technologist','role_name' => 'Lab Technologist'],
            ['role' => 'pharmacy_technician','role_name' => 'Pharmacy Technician'],
            ['role' => 'health_care_workers','role_name' => 'Health Care Department'],
        ];
        DB::table('roles')->insert($roles);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
