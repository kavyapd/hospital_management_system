<?php

use Illuminate\Database\Seeder;

class HospitalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('hospitals')->truncate();
        
        $roles = [
            ['user_id' => 1,'hospital_name' => 'Vijaynagar Hospital'],
            ['user_id' => 1,'hospital_name' => 'Narayana Multispeciality Hospital'], 
            ['user_id' => 1,'hospital_name' => 'Apollo Speciality Hospital'],
            ['user_id' => 1,'hospital_name' => 'Suguna Hospital'],
            ['user_id' => 1,'hospital_name' => 'Omega Multispeciality Hospital'],
        ];
        DB::table('hospitals')->insert($roles);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
