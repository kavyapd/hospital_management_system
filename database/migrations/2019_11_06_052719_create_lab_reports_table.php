<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lab_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('appointment_scheduled_id')->unsigned()->index();
            $table->foreign('appointment_scheduled_id')->references('id')->on('appointment_scheduled_information')->onDelete('cascade');
            $table->bigInteger('lab_reporter_id')->unsigned()->index();
            $table->foreign('lab_reporter_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('lab_report')->nullable();
            $table->dateTime('lab_report_scheduled_date_time');
            $table->enum('lab_report_status',array('active','active_saved','cancelled','no_show','complete'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lab_reporters');
    }
}
