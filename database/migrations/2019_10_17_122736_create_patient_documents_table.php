<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_documents', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->bigInteger('patient_id')->unsigned()->index()->unique();
            $table->foreign('patient_id')->references('id')->on('patient_information')->onDelete('cascade');
            $table->string('file_path')->nullable();
            $table->string('file_type')->nullable();
            $table->string('original_file_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_documents');
    }
}
