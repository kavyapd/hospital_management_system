<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHealthCareWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_care_workers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('appointment_scheduled_id')->unsigned()->index();
            $table->foreign('appointment_scheduled_id')->references('id')->on('appointment_scheduled_information')->onDelete('cascade');
            $table->bigInteger('helath_care_worker_id')->unsigned()->index();
            $table->foreign('helath_care_worker_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('helath_care_worker_report')->nullable();
            $table->enum('helath_care_worker_report_status',array('active','active_saved','cancelled','no_show','complete'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_care_workers');
    }
}
