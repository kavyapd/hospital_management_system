<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentScheduledInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_scheduled_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('appointment_id')->nullable();
            $table->bigInteger('doctor_id')->unsigned()->index()->nullable();
            $table->foreign('doctor_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('patient_id')->unsigned()->index()->nullable();
            $table->foreign('patient_id')->references('id')->on('patient_information')->onDelete('cascade');
            $table->dateTime('appointment_date_time')->nullable();
            $table->enum('appointment_status',array('active','active_saved','cancelled','no_show','complete'));
            $table->bigInteger('scheduled_by')->unsigned()->index();
            $table->foreign('scheduled_by')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_scheduled_information');
    }
}
