<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('patient_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('created_by')->unsigned()->index()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->string('patient_id')->nullable();
            $table->string('patient_name');
            $table->string('birth_date')->nullable();
            $table->bigInteger('gender')->unsigned()->index();
            $table->bigInteger('marital_status')->unsigned()->index();
            $table->bigInteger('age')->unsigned()->index();
            $table->string('occupation')->nullable();
            $table->bigInteger('mobile_number')->unsigned()->index();
            $table->string('email')->nullable();
            $table->string('language')->nullable();
            $table->string('nationality')->nullable();
            $table->bigInteger('responsible_person')->unsigned()->index()->nullable();
            $table->string('responsible_person_name')->nullable();
            $table->bigInteger('responsible_person_mobile_number')->unsigned()->index()->nullable();
            $table->string('address')->nullable();
            $table->string('refered_by_doctor')->nullable();
            $table->bigInteger('updated_by')->unsigned()->index()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_information');
    }
}
