<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePharmacyReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacy_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('appointment_scheduled_id')->unsigned()->index();
            $table->foreign('appointment_scheduled_id')->references('id')->on('appointment_scheduled_information')->onDelete('cascade');
            $table->bigInteger('pharmacy_reporter_id')->unsigned()->index();
            $table->foreign('pharmacy_reporter_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('pharmacy_report')->nullable();
            $table->enum('pharmacy_report_status',array('active','active_saved','cancelled','no_show','complete'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacy_reporters');
    }
}
