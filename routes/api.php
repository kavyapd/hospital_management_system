<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group([
    'prefix' => 'auth',
    'middleware' => ['cors','XSS']
], function(){
    Route::post('register', 'User\RegisterController@registerDetails');
    Route::get('account-activation','User\AccountActivationController@activateAccount');
    Route::post('send-sms','User\SMSController@sendSMS');
//    Route::post('send-otp','User\SMSController@sendOtp');
    Route::get('get/hospitals','User\RegisterController@getHospitalNames');
    Route::get('get/hospital-roles/details','User\RegisterController@getHospitalandRolesNames');
    Route::post('request/reset-password', 'User\ResetPassword@requestResetPassword');
    Route::post('reset-password', 'User\ResetPassword@resetPassword');
    Route::post('login','User\LoginController@login');
    Route::post('sample','SampleController@samplecode');

    Route::group([
        'middleware' => 'auth:api'
    ], function(){
         //admin dashboard
        Route::get('get/hospital-details','Admin\AdminDashboardController@gethospitalDetails');
        Route::get('get/user-details','Admin\AdminDashboardController@getUserDetails')->middleware('CheckAdminRole');
        Route::post('add/hospitals','Admin\AdminDashboardController@addHospitalDetails')->middleware('CheckAdminRole');
        Route::post('update/hospital-details','Admin\AdminDashboardController@updateHospitalsDetails')->middleware('CheckAdminRole');        
        Route::delete('delete-hospital/{id}','Admin\AdminDashboardController@deleteHospital')->where('id', '[0-9]+')->middleware('CheckAdminRole'); 
        Route::post('user-approved/{id}','Admin\ApprovedRejectController@adminApprovalStatus')->where('id', '[0-9]+')->middleware('CheckAdminRole');
        Route::post('user-rejected/{id}','Admin\ApprovedRejectController@adminRejectStatus')->where('id', '[0-9]+')->middleware('CheckAdminRole');
         //change password
        Route::put('change/old-password','User\ChangePasswordController@changePassword');
         //receptionist dashboard      
        Route::get('get/roles-details','Roles\RolesWiseDashboardController@getRolesDetails');
        Route::post('update/roles-details','Roles\RolesWiseDashboardController@updateRolesDetails')->middleware('CheckRoles');
        Route::post('add/patient-details','Roles\Receptionist\PatientInformationController@addPatientDetails')->middleware('CheckReceptionistRole');
        Route::post('update/patient-details','Roles\Receptionist\PatientInformationController@updatePatientDetails')->middleware('CheckReceptionistRole');
        Route::get('get/patient-details','Roles\Receptionist\PatientInformationController@getPatientDetails')->middleware('CheckReceptionistRole');
        Route::delete('delete-patient/{id}','Roles\Receptionist\PatientInformationController@deletePatientDetails')->where('id', '[0-9]+')->middleware('CheckReceptionistRole');         
        Route::get('get/doctor-names/{id}','Roles\Receptionist\AppointmentScheduledController@getDoctorNames')->where('id', '[0-9]+')->middleware('CheckReceptionistRole');                
        Route::post('get/patient-names','Roles\Receptionist\AppointmentScheduledController@getPatientNames')->middleware('CheckReceptionistRole');                
        Route::post('schedule/appointment','Roles\Receptionist\AppointmentScheduledController@scheduleAppointment')->middleware('CheckReceptionistRole');                
        Route::get('get/scheduled-appointments','Roles\Receptionist\AppointmentScheduledController@getScheduledAppointmentsList')->where('id', '[0-9]+')->middleware('CheckReceptionistRole');    
        Route::post('update/schedule-appointment','Roles\Receptionist\AppointmentScheduledController@updateScheduledAppointmentDetails')->middleware('CheckReceptionistRole');                
        Route::delete('delete-appointment/{id}','Roles\Receptionist\AppointmentScheduledController@deleteScheduledAppointmentDetails')->where('id', '[0-9]+')->middleware('CheckReceptionistRole'); 
        Route::post('get/date-wise/patient-details','Roles\Receptionist\PatientInformationController@getDateWisePatientDetails')->middleware('CheckReceptionistRole');                
        //doctor dashboard
        Route::get('get/doctor-wise/scheduled-appointments','Roles\Doctor\DoctorController@getScheduledAppointmentsDetailsDoctorWise')->middleware('CheckDoctorRole');    
        Route::post('assign/lab-report','Roles\Doctor\ScheduledLabPharmacyController@scheduledLabReportDetails')->middleware('CheckDoctorRole');    
        Route::post('assign/pharmacy-report','Roles\Doctor\ScheduledLabPharmacyController@scheduledPharmacyReportDetails')->middleware('CheckDoctorRole');    
        Route::get('get/lab-reporter-names/{id}','Roles\Doctor\DoctorController@getLabReporterNames')->middleware('CheckDoctorRole');    
        Route::get('get/pharmacy-reporter-names/{id}','Roles\Doctor\DoctorController@getPharmacyReporterNames')->middleware('CheckDoctorRole');    
        Route::get('get/health-care-workers-names/{id}','Roles\Doctor\DoctorController@getHelathCareWorkersNames')->middleware('CheckDoctorRole');    
        Route::post('add/vitals-details','Roles\Receptionist\PatientVitalDiagnosisController@addPatientVitalsDetails')->middleware('CheckDoctorRole');
        Route::post('add/diagnosis-details','Roles\Receptionist\PatientVitalDiagnosisController@addPatientDiagnosisDetails')->middleware('CheckDoctorRole');
        Route::get('get/document/{id}','Roles\Receptionist\PatientVitalDiagnosisController@fileDownload')->where('id', '[0-9]+'); 
        Route::post('file/upload','Roles\Receptionist\PatientVitalDiagnosisController@fileUpload')->middleware('CheckDoctorRole');
    });
});


